use std::io;
use std::time::Duration;
use crossterm::{event, execute, terminal::{disable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen}};
use crossterm::event::{Event, KeyCode, poll};
use crossterm::terminal::enable_raw_mode;
use tui::{backend::CrosstermBackend, Frame, Terminal, widgets::{Block, Borders}};
use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout};
use tui::text::Span;
use tui::widgets::{Cell, Paragraph, Row, Table};
use gb::gameboy::Gameboy;

pub fn render(mut gb: Gameboy) -> Result<(), io::Error> {
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    run_app(&mut terminal, &mut gb)?;

    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
    terminal.show_cursor()?;

    Ok(())
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, gb: &mut Gameboy) -> io::Result<()> {
    let mut running = false;

    loop {
        terminal.draw(|f| ui(f, gb))?;

        if poll(Duration::from_millis(100))? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('s') => if !running { gb.step() },
                    KeyCode::Char('r') => running = true,
                    KeyCode::Char('t') => running = false,
                    KeyCode::Esc => return Ok(()),
                    _ => {}
                }
            }
        }

        if running {
            gb.step()
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, gb: &mut Gameboy) {
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(1)
        .constraints(
            [
                Constraint::Percentage(25),
                Constraint::Percentage(50),
                Constraint::Percentage(25),
            ].as_ref()
        )
        .split(f.size());

    let rows: Vec<Row> = vec![
        Row::new(
            vec![
                Cell::from(Span::raw(format!("A: {:0>2X}", gb.cpu.reg_a))),
                Cell::from(Span::raw(format!("F: {:0>2X}", gb.cpu.reg_f))),
            ],
        ),
        Row::new(
            vec![
                Cell::from(Span::raw(format!("B: {:0>2X}", gb.cpu.reg_b))),
                Cell::from(Span::raw(format!("C: {:0>2X}", gb.cpu.reg_c))),
            ],
        ),
        Row::new(
            vec![
                Cell::from(Span::raw(format!("D: {:0>2X}", gb.cpu.reg_d))),
                Cell::from(Span::raw(format!("E: {:0>2X}", gb.cpu.reg_e))),
            ],
        ),
        Row::new(
            vec![
                Cell::from(Span::raw(format!("H: {:0>2X}", gb.cpu.reg_h))),
                Cell::from(Span::raw(format!("L: {:0>2X}", gb.cpu.reg_l))),
            ],
        ),
        Row::new(
            vec![
                Cell::from(Span::raw(format!("SP: {:0>4X}", gb.cpu.sp))),
                Cell::from(Span::raw(format!("PC: {:0>4X}", gb.cpu.pc))),
            ],
        ),
    ];

    let registers = Table::new(rows)
        .block(Block::default().title("CPU"))
        .widths(&[
            Constraint::Ratio(1, 2),
            Constraint::Ratio(1, 2),
        ]);
    f.render_widget(registers, chunks[0]);

    let instr_block = Block::default()
        .title("Instructions")
        .borders(Borders::ALL);

    let next_op = gb.lookahead();
    let instruction = Paragraph::new(format!("Next: {}", gb.disassemble(next_op)))
        .block(instr_block);
    f.render_widget(instruction, chunks[1]);

    let io_block = Block::default()
        .title("I/O")
        .borders(Borders::ALL);
    let serial = Paragraph::new(format!("Serial: {}", gb.read_mem(0xFF01)))
        .block(io_block);
    f.render_widget(serial, chunks[2]);
}
