use std::thread::sleep;
use crate::bus::Bus;
use crate::{add_signed, bit, byte_merge, byte_split, reset_bit, set_bit};

#[derive(Clone,Copy,Debug)]
#[allow(non_camel_case_types)]
pub enum Op {
    // 8-bit loads
    LD_n_n(ReadByte, ReadByte),
    LD_A_n(LdASrc),
    LD_n_A(LdADest),
    LD_A_C,
    LD_C_A,
    LD_nn_n(LdRegDest),
    LDD_A_HL,
    LDD_HL_A,
    LDI_A_HL,
    LDI_HL_A,
    LDH_n_A,
    LDH_A_n,

    // 16-bit loads
    LD_n_nn(WordRegDest),
    LD_SP_HL,
    LDHL_SP_n,
    LD_nn_SP,
    PUSH_nn(JumpWord),
    POP_nn(JumpWord),

    // 8-bit ALU
    ADD_A_n(ReadByte),
    ADC_A_n(ReadByte),
    SUB_A_n(ReadByte),
    SBC_A_n(ReadByte),
    AND_A_n(ReadByte),
    OR_A_n(ReadByte),
    XOR_A_n(ReadByte),
    CP_A_n(ReadByte),
    INC_n(ReadByte),
    DEC_A_n(ReadByte),

    // 16-bit ALU
    ADD_HL_n(ReadWord),
    ADD_SP_n,
    INC_nn(ReadWord),
    DEC_nn(ReadWord),

    // Miscellaneous
    SWAP,
    DAA,
    CPL,
    CCF,
    SCF,
    NOP,
    HALT,
    STOP,
    EI,
    DI,
    Prefix,

    // Rotates & Shifts
    RLCA,
    RLA,
    RRCA,
    RRA,
    RLC_n(RotateByte),
    RL_n(RotateByte),
    RRC_n(RotateByte),
    RR_n(RotateByte),
    SLA_n(RotateByte),
    SRA_n(RotateByte),
    SRL_n(RotateByte),

    // Bit Opcodes
    BIT(BitByte),
    SET(BitByte),
    RES(BitByte),

    // Jumps
    JP_nn,
    JP_cc_nn(JumpCond),
    JP_HL,
    JR_n,
    JR_cc_n(JumpCond),

    // Calls
    CALL_nn,
    CALL_cc_nn(JumpCond),

    // Restarts
    RST(RstAddr),

    // Returns
    RET,
    RET_cc(JumpCond),
    RETI,
}

/// Load Register Destination
#[derive(Clone,Copy,Debug)]
pub enum LdRegDest {
    B, C, D, E, H, L,
}

#[derive(Clone,Copy,Debug,PartialEq)]
pub enum ReadByte {
    A, B, C, D, E, H, L,
    AtHL,
    Next,
}

#[derive(Clone,Copy,Debug)]
pub enum WordRegDest {
    BC, DE, HL, SP,
}

#[derive(Clone,Copy,Debug)]
pub enum ReadWord {
    BC, DE, HL, SP,
}

#[derive(Clone,Copy,Debug)]
pub enum BitByte {
    A, B, C, D, E, H, L,
    AtHL,
}

/// Jump Condition
#[derive(Clone,Copy,Debug)]
pub enum JumpCond {
    NZ, Z, NC, C,
}

/// Load A Destination
#[derive(Clone,Copy,Debug)]
pub enum LdADest {
    A, B, C, D, E, H, L,
    AtBC, AtDE, AtHL,
    AtNext,
}

/// Load A Source
#[derive(Clone,Copy,Debug)]
pub enum LdASrc {
    A, B, C, D, E, H, L,
    AtBC, AtDE, AtHL,
    AtNext,
}

#[derive(Clone,Copy,Debug)]
pub enum JumpWord {
    AF, BC, DE, HL,
}

#[derive(Clone,Copy,Debug)]
pub enum RotateByte {
    A, B, C, D, E, H, L,
    AtHL,
}

/// Restart Address
#[derive(Clone,Copy,Debug)]
pub enum RstAddr {
    X00, X08,
    X10, X18,
    X20, X28,
    X30, X38,
}

pub struct Cpu {
    pub reg_a: u8,
    pub reg_f: u8,
    pub reg_b: u8,
    pub reg_c: u8,
    pub reg_d: u8,
    pub reg_e: u8,
    pub reg_h: u8,
    pub reg_l: u8,

    /// Program Counter
    pub sp: u16,

    /// Stack Pointer
    pub pc: u16,

    stopped: bool,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            reg_a: 0,
            reg_f: 0,
            reg_b: 0,
            reg_c: 0,
            reg_d: 0,
            reg_e: 0,
            reg_h: 0,
            reg_l: 0,

            sp: 0,
            pc: 0x0100,

            stopped: false,
        }
    }

    pub fn step(&mut self, bus: &Bus) {
        if self.stopped {
            return;
        }

        let op_code = bus.read(self.pc);
        let op = Self::map_opcode(op_code);
        self.execute(op, bus);
    }

    pub fn lookahead(&self, bus: &Bus) -> Op {
        let op_code = bus.read(self.pc);
        Self::map_opcode(op_code)
    }

    fn map_opcode(code: u8) -> Op {
        match code {
            0x00 => Op::NOP,
            0x01 => Op::LD_n_nn(WordRegDest::BC),
            0x02 => Op::LD_n_A(LdADest::AtBC),
            0x03 => Op::INC_nn(ReadWord::BC),
            0x04 => Op::INC_n(ReadByte::B),
            0x05 => Op::DEC_A_n(ReadByte::B),
            0x06 => Op::LD_n_n(ReadByte::B, ReadByte::Next),
            0x07 => Op::RLCA,
            0x08 => Op::LD_nn_SP,
            0x09 => Op::ADD_HL_n(ReadWord::BC),
            0x0A => Op::LD_A_n(LdASrc::AtBC),
            0x0B => Op::DEC_nn(ReadWord::BC),
            0x0C => Op::INC_n(ReadByte::C),
            0x0D => Op::DEC_A_n(ReadByte::C),
            0x0E => Op::LD_n_n(ReadByte::C, ReadByte::Next),
            0x0F => Op::RRCA,

            0x10 => Op::STOP,
            0x11 => Op::LD_n_nn(WordRegDest::DE),
            0x12 => Op::LD_n_A(LdADest::AtDE),
            0x13 => Op::INC_nn(ReadWord::DE),
            0x14 => Op::INC_n(ReadByte::D),
            0x15 => Op::DEC_A_n(ReadByte::D),
            0x16 => Op::LD_n_n(ReadByte::D, ReadByte::Next),
            0x17 => Op::RLA,
            0x18 => Op::JR_n,
            0x19 => Op::ADD_HL_n(ReadWord::DE),
            0x1A => Op::LD_A_n(LdASrc::AtDE),
            0x1B => Op::DEC_nn(ReadWord::DE),
            0x1C => Op::INC_n(ReadByte::E),
            0x1D => Op::DEC_A_n(ReadByte::E),
            0x1E => Op::LD_n_n(ReadByte::E, ReadByte::Next),
            0x1F => Op::RRA,

            0x20 => Op::JR_cc_n(JumpCond::NZ),
            0x21 => Op::LD_n_nn(WordRegDest::HL),
            0x22 => Op::LDI_HL_A,
            0x23 => Op::INC_nn(ReadWord::HL),
            0x24 => Op::INC_n(ReadByte::H),
            0x25 => Op::DEC_A_n(ReadByte::H),
            0x26 => Op::LD_n_n(ReadByte::H, ReadByte::Next),
            0x27 => Op::DAA,
            0x28 => Op::JR_cc_n(JumpCond::Z),
            0x29 => Op::ADD_HL_n(ReadWord::HL),
            0x2A => Op::LDI_A_HL,
            0x2B => Op::DEC_nn(ReadWord::HL),
            0x2C => Op::INC_n(ReadByte::L),
            0x2D => Op::DEC_A_n(ReadByte::L),
            0x2E => Op::LD_n_n(ReadByte::L, ReadByte::Next),
            0x2F => Op::CPL,

            0x30 => Op::JR_cc_n(JumpCond::NC),
            0x31 => Op::LD_n_nn(WordRegDest::SP),
            0x32 => Op::LDD_HL_A,
            0x33 => Op::INC_nn(ReadWord::SP),
            0x34 => Op::INC_n(ReadByte::AtHL),
            0x35 => Op::DEC_A_n(ReadByte::AtHL),
            0x36 => Op::LD_n_n(ReadByte::AtHL, ReadByte::Next),
            0x37 => Op::SCF,
            0x38 => Op::JR_cc_n(JumpCond::C),
            0x39 => Op::ADD_HL_n(ReadWord::SP),
            0x3A => Op::LDD_A_HL,
            0x3B => Op::DEC_nn(ReadWord::SP),
            0x3C => Op::INC_n(ReadByte::A),
            0x3D => Op::DEC_A_n(ReadByte::A),
            0x3E => Op::LD_n_n(ReadByte::A, ReadByte::Next),
            0x3F => Op::CCF,

            0x40 => Op::LD_n_n(ReadByte::B, ReadByte::B),
            0x41 => Op::LD_n_n(ReadByte::B, ReadByte::C),
            0x42 => Op::LD_n_n(ReadByte::B, ReadByte::D),
            0x43 => Op::LD_n_n(ReadByte::B, ReadByte::E),
            0x44 => Op::LD_n_n(ReadByte::B, ReadByte::H),
            0x45 => Op::LD_n_n(ReadByte::B, ReadByte::L),
            0x46 => Op::LD_n_n(ReadByte::B, ReadByte::AtHL),
            0x47 => Op::LD_n_n(ReadByte::B, ReadByte::A),
            0x48 => Op::LD_n_n(ReadByte::C, ReadByte::B),
            0x49 => Op::LD_n_n(ReadByte::C, ReadByte::C),
            0x4A => Op::LD_n_n(ReadByte::C, ReadByte::D),
            0x4B => Op::LD_n_n(ReadByte::C, ReadByte::E),
            0x4C => Op::LD_n_n(ReadByte::C, ReadByte::H),
            0x4D => Op::LD_n_n(ReadByte::C, ReadByte::L),
            0x4E => Op::LD_n_n(ReadByte::C, ReadByte::AtHL),
            0x4F => Op::LD_n_n(ReadByte::C, ReadByte::A),

            0x50 => Op::LD_n_n(ReadByte::D, ReadByte::B),
            0x51 => Op::LD_n_n(ReadByte::D, ReadByte::C),
            0x52 => Op::LD_n_n(ReadByte::D, ReadByte::D),
            0x53 => Op::LD_n_n(ReadByte::D, ReadByte::E),
            0x54 => Op::LD_n_n(ReadByte::D, ReadByte::H),
            0x55 => Op::LD_n_n(ReadByte::D, ReadByte::L),
            0x56 => Op::LD_n_n(ReadByte::D, ReadByte::AtHL),
            0x57 => Op::LD_n_n(ReadByte::D, ReadByte::A),
            0x58 => Op::LD_n_n(ReadByte::E, ReadByte::B),
            0x59 => Op::LD_n_n(ReadByte::E, ReadByte::C),
            0x5A => Op::LD_n_n(ReadByte::E, ReadByte::D),
            0x5B => Op::LD_n_n(ReadByte::E, ReadByte::E),
            0x5C => Op::LD_n_n(ReadByte::E, ReadByte::H),
            0x5D => Op::LD_n_n(ReadByte::E, ReadByte::L),
            0x5E => Op::LD_n_n(ReadByte::E, ReadByte::AtHL),
            0x5F => Op::LD_n_n(ReadByte::E, ReadByte::A),

            0x60 => Op::LD_n_n(ReadByte::H, ReadByte::B),
            0x61 => Op::LD_n_n(ReadByte::H, ReadByte::C),
            0x62 => Op::LD_n_n(ReadByte::H, ReadByte::D),
            0x63 => Op::LD_n_n(ReadByte::H, ReadByte::E),
            0x64 => Op::LD_n_n(ReadByte::H, ReadByte::H),
            0x65 => Op::LD_n_n(ReadByte::H, ReadByte::L),
            0x66 => Op::LD_n_n(ReadByte::H, ReadByte::AtHL),
            0x67 => Op::LD_n_n(ReadByte::H, ReadByte::A),
            0x68 => Op::LD_n_n(ReadByte::L, ReadByte::B),
            0x69 => Op::LD_n_n(ReadByte::L, ReadByte::C),
            0x6A => Op::LD_n_n(ReadByte::L, ReadByte::D),
            0x6B => Op::LD_n_n(ReadByte::L, ReadByte::E),
            0x6C => Op::LD_n_n(ReadByte::L, ReadByte::H),
            0x6D => Op::LD_n_n(ReadByte::L, ReadByte::L),
            0x6E => Op::LD_n_n(ReadByte::L, ReadByte::AtHL),
            0x6F => Op::LD_n_n(ReadByte::L, ReadByte::A),

            0x70 => Op::LD_n_n(ReadByte::AtHL, ReadByte::B),
            0x71 => Op::LD_n_n(ReadByte::AtHL, ReadByte::C),
            0x72 => Op::LD_n_n(ReadByte::AtHL, ReadByte::D),
            0x73 => Op::LD_n_n(ReadByte::AtHL, ReadByte::E),
            0x74 => Op::LD_n_n(ReadByte::AtHL, ReadByte::H),
            0x75 => Op::LD_n_n(ReadByte::AtHL, ReadByte::L),
            0x76 => Op::HALT,
            0x77 => Op::LD_n_n(ReadByte::AtHL, ReadByte::A),
            0x78 => Op::LD_n_n(ReadByte::A, ReadByte::B),
            0x79 => Op::LD_n_n(ReadByte::A, ReadByte::C),
            0x7A => Op::LD_n_n(ReadByte::A, ReadByte::D),
            0x7B => Op::LD_n_n(ReadByte::A, ReadByte::E),
            0x7C => Op::LD_n_n(ReadByte::A, ReadByte::H),
            0x7D => Op::LD_n_n(ReadByte::A, ReadByte::L),
            0x7E => Op::LD_n_n(ReadByte::A, ReadByte::AtHL),
            0x7F => Op::LD_n_n(ReadByte::A, ReadByte::A),

            0x80 => Op::ADD_A_n(ReadByte::B),
            0x81 => Op::ADD_A_n(ReadByte::C),
            0x82 => Op::ADD_A_n(ReadByte::D),
            0x83 => Op::ADD_A_n(ReadByte::E),
            0x84 => Op::ADD_A_n(ReadByte::H),
            0x85 => Op::ADD_A_n(ReadByte::L),
            0x86 => Op::ADD_A_n(ReadByte::AtHL),
            0x87 => Op::ADD_A_n(ReadByte::A),
            0x88 => Op::ADC_A_n(ReadByte::B),
            0x89 => Op::ADC_A_n(ReadByte::C),
            0x8A => Op::ADC_A_n(ReadByte::D),
            0x8B => Op::ADC_A_n(ReadByte::E),
            0x8C => Op::ADC_A_n(ReadByte::H),
            0x8D => Op::ADC_A_n(ReadByte::L),
            0x8E => Op::ADC_A_n(ReadByte::AtHL),
            0x8F => Op::ADC_A_n(ReadByte::A),

            0x90 => Op::SUB_A_n(ReadByte::B),
            0x91 => Op::SUB_A_n(ReadByte::C),
            0x92 => Op::SUB_A_n(ReadByte::D),
            0x93 => Op::SUB_A_n(ReadByte::E),
            0x94 => Op::SUB_A_n(ReadByte::H),
            0x95 => Op::SUB_A_n(ReadByte::L),
            0x96 => Op::SUB_A_n(ReadByte::AtHL),
            0x97 => Op::SUB_A_n(ReadByte::A),
            0x98 => Op::SBC_A_n(ReadByte::B),
            0x99 => Op::SBC_A_n(ReadByte::C),
            0x9A => Op::SBC_A_n(ReadByte::D),
            0x9B => Op::SBC_A_n(ReadByte::E),
            0x9C => Op::SBC_A_n(ReadByte::H),
            0x9D => Op::SBC_A_n(ReadByte::L),
            0x9E => Op::SBC_A_n(ReadByte::AtHL),
            0x9F => Op::SBC_A_n(ReadByte::A),

            0xA0 => Op::AND_A_n(ReadByte::B),
            0xA1 => Op::AND_A_n(ReadByte::C),
            0xA2 => Op::AND_A_n(ReadByte::D),
            0xA3 => Op::AND_A_n(ReadByte::E),
            0xA4 => Op::AND_A_n(ReadByte::H),
            0xA5 => Op::AND_A_n(ReadByte::L),
            0xA6 => Op::AND_A_n(ReadByte::AtHL),
            0xA7 => Op::AND_A_n(ReadByte::A),
            0xA8 => Op::XOR_A_n(ReadByte::B),
            0xA9 => Op::XOR_A_n(ReadByte::C),
            0xAA => Op::XOR_A_n(ReadByte::D),
            0xAB => Op::XOR_A_n(ReadByte::E),
            0xAC => Op::XOR_A_n(ReadByte::H),
            0xAD => Op::XOR_A_n(ReadByte::L),
            0xAE => Op::XOR_A_n(ReadByte::AtHL),
            0xAF => Op::XOR_A_n(ReadByte::A),

            0xB0 => Op::OR_A_n(ReadByte::B),
            0xB1 => Op::OR_A_n(ReadByte::C),
            0xB2 => Op::OR_A_n(ReadByte::D),
            0xB3 => Op::OR_A_n(ReadByte::E),
            0xB4 => Op::OR_A_n(ReadByte::H),
            0xB5 => Op::OR_A_n(ReadByte::L),
            0xB6 => Op::OR_A_n(ReadByte::AtHL),
            0xB7 => Op::OR_A_n(ReadByte::A),
            0xB8 => Op::CP_A_n(ReadByte::B),
            0xB9 => Op::CP_A_n(ReadByte::C),
            0xBA => Op::CP_A_n(ReadByte::D),
            0xBB => Op::CP_A_n(ReadByte::E),
            0xBC => Op::CP_A_n(ReadByte::H),
            0xBD => Op::CP_A_n(ReadByte::L),
            0xBE => Op::CP_A_n(ReadByte::AtHL),
            0xBF => Op::CP_A_n(ReadByte::A),

            0xC0 => Op::RET_cc(JumpCond::NZ),
            0xC1 => Op::POP_nn(JumpWord::BC),
            0xC2 => Op::JP_cc_nn(JumpCond::NZ),
            0xC3 => Op::JP_nn,
            0xC4 => Op::CALL_cc_nn(JumpCond::NZ),
            0xC5 => Op::PUSH_nn(JumpWord::BC),
            0xC6 => Op::ADD_A_n(ReadByte::Next),
            0xC7 => Op::RST(RstAddr::X00),
            0xC8 => Op::RET_cc(JumpCond::Z),
            0xC9 => Op::RET,
            0xCA => Op::JP_cc_nn(JumpCond::Z),
            0xCB => Op::Prefix,
            0xCC => Op::CALL_cc_nn(JumpCond::Z),
            0xCD => Op::CALL_nn,
            0xCE => Op::ADC_A_n(ReadByte::Next),
            0xCF => Op::RST(RstAddr::X08),

            0xD0 => Op::RET_cc(JumpCond::NC),
            0xD1 => Op::POP_nn(JumpWord::DE),
            0xD2 => Op::JP_cc_nn(JumpCond::NC),
            //
            0xD4 => Op::CALL_cc_nn(JumpCond::NC),
            0xD5 => Op::PUSH_nn(JumpWord::DE),
            0xD6 => Op::SUB_A_n(ReadByte::Next),
            0xD7 => Op::RST(RstAddr::X10),
            0xD8 => Op::RET_cc(JumpCond::C),
            0xD9 => Op::RETI,
            0xDA => Op::JP_cc_nn(JumpCond::C),
            //
            0xDC => Op::CALL_cc_nn(JumpCond::C),
            //
            0xDE => Op::SBC_A_n(ReadByte::Next),
            0xDF => Op::RST(RstAddr::X18),

            0xE0 => Op::LDH_n_A,
            0xE1 => Op::POP_nn(JumpWord::HL),
            0xE2 => Op::LD_C_A,
            //
            //
            0xE5 => Op::PUSH_nn(JumpWord::HL),
            0xE6 => Op::AND_A_n(ReadByte::Next),
            0xE7 => Op::RST(RstAddr::X20),
            0xE8 => Op::ADD_SP_n,
            0xE9 => Op::JP_HL,
            0xEA => Op::LD_n_A(LdADest::AtNext),
            //
            //
            //
            0xEE => Op::XOR_A_n(ReadByte::Next),
            0xEF => Op::RST(RstAddr::X28),

            0xF0 => Op::LDH_A_n,
            0xF1 => Op::POP_nn(JumpWord::AF),
            0xF2 => Op::LD_A_C,
            0xF3 => Op::DI,
            //
            0xF5 => Op::PUSH_nn(JumpWord::AF),
            0xF6 => Op::OR_A_n(ReadByte::Next),
            0xF7 => Op::RST(RstAddr::X30),
            0xF8 => Op::LDHL_SP_n,
            0xF9 => Op::LD_SP_HL,
            0xFA => Op::LD_A_n(LdASrc::AtNext),
            0xFB => Op::EI,
            //
            //
            0xFE => Op::CP_A_n(ReadByte::Next),
            0xFF => Op::RST(RstAddr::X38),

            _ => panic!("Opcode not supported: {:0>2X}", code),
        }
    }

    pub fn disassemble(&self, op: Op, bus: &Bus) -> String {
        match op {
            Op::ADC_A_n(n) =>
                format!("ADC A,#{:0>2X}", self.read_byte(n, bus)),
            Op::ADD_A_n(n) => {
                if n == ReadByte::Next {
                    let v = self.read_byte(n, bus);
                    format!("ADD A,{:0>2X}", v)
                } else {
                    format!("ADD A,{:?}", n)
                }
            }
            Op::AND_A_n(n) => {
                if n == ReadByte::Next {
                    let v = self.read_byte(n, bus);
                    format!("ADD A,{:0>2X}", v)
                } else {
                    format!("AND A,{:?}", n)
                }
            }
            Op::CALL_cc_nn(cc) => format!("CALL {:?} ${:0>4X}", cc, self.read_next_word(bus)),
            Op::CALL_nn => format!("CALL ${:0>4X}", self.read_next_word(bus)),
            Op::DEC_A_n(n) => format!("DEC {:?}", n),
            Op::DEC_nn(nn) => format!("DEC {:?}", nn),
            Op::INC_n(n) => format!("INC {:?}", n),
            Op::INC_nn(nn) => format!("INC {:?}", nn),
            Op::JP_nn => format!("JP ${:0>4X}", self.read_next_word(bus)),
            Op::JP_cc_nn(cc) => format!("JP {:?},${:0>4X}", cc, self.read_next_word(bus)),
            Op::JR_cc_n(cc) => format!("JR {:?},${:0>2X}", cc, self.read_next_byte(bus)),
            Op::LD_n_A(d) => format!("LD {:?},A", d),
            Op::LD_n_n(n1, n2) => {
                let src = match n2 {
                    ReadByte::Next => format!("{:0>2X}", self.read_next_byte(bus)),
                    _ => format!("{:?}", n2)
                };

                format!("LD {:?},{}", n1, src)
            },
            Op::LD_n_nn(n) => {
                let val = format!("{:0>4X}", self.read_next_word(bus));

                format!("LD {:?},${}", n, val)
            },
            Op::LD_nn_SP => format!("LD ({:0>4X}),SP", self.read_next_word(bus)),
            Op::LDI_A_HL => format!("LDI A,(HL)"),
            Op::NOP => format!("NOP"),
            Op::STOP => format!("STOP"),

            _ => { unreachable!("what is this: {:?}", op)}
        }
    }

    fn execute(&mut self, op: Op, bus: &Bus) {
        // self.disassemble(op, bus);

        match op {
            Op::DEC_A_n(r) => {
                let sum = self.byte_sub(r, 1, bus);
                match r {
                    ReadByte::A => self.reg_a = sum,
                    ReadByte::B => self.reg_b = sum,
                    ReadByte::C => self.reg_c = sum,
                    ReadByte::D => self.reg_d = sum,
                    ReadByte::E => self.reg_e = sum,
                    ReadByte::H => self.reg_h = sum,
                    ReadByte::L => self.reg_l = sum,
                    ReadByte::AtHL => bus.write(self.read_hl(), sum),
                    ReadByte::Next => unreachable!("Cannot decrement next."),
                }

                self.pc += 1;
            }
            Op::INC_n(r) => {
                let sum = self.byte_add(r, 1, bus);
                match r {
                    ReadByte::A => self.reg_a = sum,
                    ReadByte::B => self.reg_b = sum,
                    ReadByte::C => self.reg_c = sum,
                    ReadByte::D => self.reg_d = sum,
                    ReadByte::E => self.reg_e = sum,
                    ReadByte::H => self.reg_h = sum,
                    ReadByte::L => self.reg_l = sum,
                    ReadByte::AtHL => bus.write(self.read_hl(), sum),
                    ReadByte::Next => unreachable!("Cannot increment next."),
                }

                self.pc += 1;
            }
            Op::JP_nn => { self.pc = self.read_next_word(bus) }
            Op::JR_cc_n(cc) => {
                let jump = match cc {
                    JumpCond::NZ => !self.flag_zero(),
                    JumpCond::Z => self.flag_zero(),
                    JumpCond::NC => !self.flag_carry(),
                    JumpCond::C => self.flag_carry(),
                };

                let offset = if jump { self.read_next_byte(bus) as i8 } else { 2 };

                self.pc = add_signed(self.pc, offset);
            }
            Op::LD_n_A(d) => {
                let mut steps = 1;
                let v = self.reg_a;
                match d {
                    LdADest::A => self.reg_a = v,
                    LdADest::B => self.reg_b = v,
                    LdADest::C => self.reg_c = v,
                    LdADest::D => self.reg_d = v,
                    LdADest::E => self.reg_e = v,
                    LdADest::H => self.reg_h = v,
                    LdADest::L => self.reg_l = v,
                    LdADest::AtBC => bus.write(self.read_bc(), v),
                    LdADest::AtDE => bus.write(self.read_de(), v),
                    LdADest::AtHL => bus.write(self.read_hl(), v),
                    LdADest::AtNext => {
                        bus.write(self.read_next_word(bus), v);
                        steps += 2;
                    }
                }

                self.pc += steps;
            }
            Op::LD_n_n(r1, r2) => {
                let mut steps = 1;

                let v = match r2 {
                    ReadByte::A => self.reg_a,
                    ReadByte::B => self.reg_b,
                    ReadByte::C => self.reg_c,
                    ReadByte::D => self.reg_d,
                    ReadByte::E => self.reg_e,
                    ReadByte::H => self.reg_h,
                    ReadByte::L => self.reg_l,
                    ReadByte::AtHL => bus.read(self.read_hl()),
                    ReadByte::Next => { steps += 1; self.read_next_byte(bus) },
                };

                match r1 {
                    ReadByte::A => self.reg_a = v,
                    ReadByte::B => self.reg_b = v,
                    ReadByte::C => self.reg_c = v,
                    ReadByte::D => self.reg_d = v,
                    ReadByte::E => self.reg_e = v,
                    ReadByte::H => self.reg_h = v,
                    ReadByte::L => self.reg_l = v,
                    ReadByte::AtHL => bus.write(self.read_hl(), v),
                    ReadByte::Next => unreachable!("Target not supported."),
                }

                self.pc += steps;
            }
            Op::LD_n_nn(r) => {
                let nn = self.read_next_word(bus);
                match r {
                    WordRegDest::BC => self.write_bc(nn),
                    WordRegDest::DE => self.write_de(nn),
                    WordRegDest::HL => self.write_hl(nn),
                    WordRegDest::SP => self.sp = nn,
                };

                self.pc += 3;
            },
            Op::LDI_A_HL => {
                self.reg_a = bus.read(self.read_hl()) + 1;
                self.pc += 1;
            }
            Op::NOP => { self.pc += 1 }
            Op::STOP => { self.stopped = true; self.pc += 1; }
            _ => todo!("Execute op: {:?}", op),
        }
    }

    fn read_byte(&self, target: ReadByte, bus: &Bus) -> u8 {
        match target {
            ReadByte::A => self.reg_a,
            ReadByte::B => self.reg_b,
            ReadByte::C => self.reg_c,
            ReadByte::D => self.reg_d,
            ReadByte::E => self.reg_e,
            ReadByte::H => self.reg_h,
            ReadByte::L => self.reg_l,
            ReadByte::AtHL => bus.read(self.read_hl()),
            ReadByte::Next => bus.read(self.pc + 1),
        }
    }

    fn read_register(&self, r: LdRegDest) -> u8 {
        match r {
            LdRegDest::B => self.reg_b,
            LdRegDest::C => self.reg_c,
            LdRegDest::D => self.reg_d,
            LdRegDest::E => self.reg_e,
            LdRegDest::H => self.reg_h,
            LdRegDest::L => self.reg_l,
        }
    }

    fn read_word(&self, target: ReadWord) -> u16 {
        match target {
            ReadWord::BC => 1,
            ReadWord::DE => 1,
            ReadWord::HL => 1,
            ReadWord::SP => 1,
        }
    }

    fn read_next_byte(&self, bus: &Bus) -> u8 {
        bus.read(self.pc + 1)
    }

    fn read_next_word(&self, bus: &Bus) -> u16 {
        let low = bus.read(self.pc + 1);
        let high = bus.read(self.pc + 2);
        byte_merge(high, low)
    }

    fn read_bc(&self) -> u16 {
        byte_merge(self.reg_b, self.reg_c)
    }

    fn write_bc(&mut self, v: u16) {
        (self.reg_b, self.reg_c) = byte_split(v);
    }

    fn read_de(&self) -> u16 {
        byte_merge(self.reg_d, self.reg_e)
    }

    fn write_de(&mut self, v: u16) {
        (self.reg_d, self.reg_e) = byte_split(v);
    }

    fn read_hl(&self) -> u16 {
        byte_merge(self.reg_h, self.reg_l)
    }

    fn write_hl(&mut self, v: u16) {
        (self.reg_h, self.reg_l) = byte_split(v);
    }

    fn byte_add(&mut self, a: ReadByte, b: u8, bus: &Bus) -> u8
    {
        let a = match a {
            ReadByte::A => self.reg_a,
            ReadByte::B => self.reg_b,
            ReadByte::C => self.reg_c,
            ReadByte::D => self.reg_d,
            ReadByte::E => self.reg_e,
            ReadByte::H => self.reg_h,
            ReadByte::L => self.reg_l,
            ReadByte::AtHL => bus.read(self.read_hl()),
            ReadByte::Next => self.read_next_byte(bus),
        };

        let s = a.wrapping_add(b);

        if s == 0 { self.set_flag_zero() } else { self.reset_flag_zero() };

        s
    }

    fn byte_sub(&mut self, a: ReadByte, b: u8, bus: &Bus) -> u8
    {
        let a = match a {
            ReadByte::A => self.reg_a,
            ReadByte::B => self.reg_b,
            ReadByte::C => self.reg_c,
            ReadByte::D => self.reg_d,
            ReadByte::E => self.reg_e,
            ReadByte::H => self.reg_h,
            ReadByte::L => self.reg_l,
            ReadByte::AtHL => bus.read(self.read_hl()),
            ReadByte::Next => self.read_next_byte(bus),
        };

        let s = a.wrapping_sub(b);

        if s == 0 { self.set_flag_zero() } else { self.reset_flag_zero() };

        s
    }

    // flags

    fn flag_zero(&self) -> bool {
        bit(self.reg_f, 7)
    }

    fn reset_flag_zero(&mut self) {
        self.reg_f = reset_bit(self.reg_f, 7)
    }

    fn set_flag_zero(&mut self) {
        self.reg_f = set_bit(self.reg_f, 7)
    }

    fn flag_carry(&self) -> bool {
        bit(self.reg_f, 4)
    }

    fn reset_flag_carry(&mut self) {
        self.reg_f = reset_bit(self.reg_f, 4)
    }

    fn set_flag_carry(&mut self) {
        self.reg_f = set_bit(self.reg_f, 4)
    }
}
