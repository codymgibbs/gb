use crate::bus::Bus;
use crate::cpu::{Cpu, Op};
use crate::rom::Rom;

pub struct Gameboy {
    bus: Bus,
    pub cpu: Cpu,
}

impl Gameboy {
    pub fn new(rom: Rom) -> Gameboy {
        let bus = Bus::new(rom);

        Gameboy {
            bus,
            cpu: Cpu::new(),
        }
    }

    pub fn disassemble(&self, op: Op) -> String {
        self.cpu.disassemble(op, &self.bus)
    }

    pub fn lookahead(&self) -> Op {
        self.cpu.lookahead(&self.bus)
    }

    pub fn step(&mut self) {
        self.cpu.step(&self.bus);
    }

    pub fn power_on(&mut self) {
        loop {
            self.step();
        }
    }

    pub fn read_mem(&self, addr: u16) -> u8 {
        self.bus.read(addr)
    }
}
