mod tui;

use std::{env, io};
use std::path::Path;
use gb::gameboy::Gameboy;
use gb::rom::Rom;

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();
    let path = Path::new(&args[1]);

    if let Ok(rom) = Rom::from_file(path) {
        println!("Playing {}", rom.game_title());
        let mut gb = Gameboy::new(rom);
        gb.power_on();
        // tui::render(gb)?;
    } else {
        println!("Unable to find rom: {:?}", path);
    }

    println!("Bye!");

    Ok(())
}
