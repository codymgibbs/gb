use crate::rom::Rom;

pub struct Bus {
    rom: Rom,
    ram: Vec<u8>,
    vram: Vec<u8>,
}

impl Bus {
    pub fn new(rom: Rom) -> Bus {
        Bus {
            rom,
            ram: vec![0; 8_192], // 8KiB
            vram: vec![0; 8_192], // 8KiB
        }
    }

    pub fn read(&self, addr: u16) -> u8 {
        match addr {
            0x0000..=0x3FFF => self.rom.read(addr),
            0x8000..=0x9FFF => self.vram[(addr - 0x8000) as usize],
            0xC000..=0xDFFF => self.ram[(addr - 0xC000) as usize],

            _ => 0x00,
        }
    }

    pub fn write(&self, addr: u16, v: u8) {
        match addr {
            _ => {}
        }
    }
}
