use std::fs::File;
use std::io::Read;
use std::path::Path;

pub struct Rom {
    data: Vec<u8>,
}

impl Rom {
    pub fn from_file(path: &Path) -> Result<Self, &'static str> {
        let mut buf = Vec::new();

        File::open(path)
            .and_then(|mut f| f.read_to_end(&mut buf))
            .map_err(|_| "Could not read ROM")?;

        Ok(
            Self {
                data: buf
            }
        )
    }

    pub fn game_title(&self) -> &str {
        let title = &self.data[0x0134..=0x0143];

        match std::str::from_utf8(&title) {
            Ok(title) => title,
            Err(_) => "[err]",
        }
    }

    pub fn read(&self, addr: u16) -> u8 {
        self.data[addr as usize]
    }
}
