mod bus;
mod cpu;
pub mod gameboy;
pub mod rom;

pub fn byte_merge(a: u8, b: u8) -> u16 {
    ((a as u16) << 8) | (b as u16)
}

pub fn byte_split(v: u16) -> (u8, u8) {
    ((v >> 8) as u8, v as u8)
}

pub fn bit(v: u8, i: u8) -> bool {
    let mask = match i {
        0 => 0x01,
        1 => 0x02,
        2 => 0x04,
        3 => 0x08,
        4 => 0x10,
        5 => 0x20,
        6 => 0x40,
        7 => 0x80,
        _ => return false,
    };

    v & mask > 0
}

pub fn reset_bit(v: u8, i: u8) -> u8 {
    let mask = match i {
        0 => 0x01,
        1 => 0x02,
        2 => 0x04,
        3 => 0x08,
        4 => 0x10,
        5 => 0x20,
        6 => 0x40,
        7 => 0x80,
        _ => return v,
    };

    v & !mask
}

pub fn set_bit(v: u8, i: u8) -> u8 {
    let mask = match i {
        0 => 0x01,
        1 => 0x02,
        2 => 0x04,
        3 => 0x08,
        4 => 0x10,
        5 => 0x20,
        6 => 0x40,
        7 => 0x80,
        _ => return v,
    };

    v | mask
}

pub fn add_signed(a: u16, b: i8) -> u16 {
    a.wrapping_add(b as u16)
}

#[cfg(test)]
mod tests {
    use crate::{byte_merge, byte_split, bit, add_signed, set_bit, reset_bit};

    #[test]
    fn test_byte_merge() {
        assert_eq!(0x1234, byte_merge(0x12, 0x34))
    }

    #[test]
    fn test_byte_split() {
        assert_eq!((0xAB, 0xCD), byte_split(0xABCD))
    }

    #[test]
    fn test_bit() {
        assert_eq!(true, bit(0xA0, 7));
        assert_eq!(false, bit(0xA0, 6));
        assert_eq!(true, bit(0xA0, 5));
        assert_eq!(false, bit(0xA0, 4));
    }

    #[test]
    fn test_reset_bit() {
        assert_eq!(0xF0, reset_bit(0xF4, 2));
        assert_eq!(0xF0, reset_bit(0xF0, 10));
    }

    #[test]
    fn test_set_bit() {
        assert_eq!(0xF4, set_bit(0xF0, 2));
        assert_eq!(0xF0, set_bit(0xF0, 10));
    }

    #[test]
    fn test_add_signed() {
        assert_eq!(7, add_signed(10, -3));
        assert_eq!(60, add_signed(10, 50u8 as i8));
        assert_eq!(5, add_signed(10, 0xFBu16 as i8));
    }
}
